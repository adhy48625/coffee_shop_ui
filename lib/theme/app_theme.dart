import 'package:flutter/material.dart';

class AppTheme {
  static final themeData = ThemeData(
    scaffoldBackgroundColor: const Color(0xFFEDDBC5),
    textTheme: const TextTheme(
      bodyLarge: TextStyle(color: Color(0xFF230C02), fontSize: 36, fontWeight: FontWeight.bold),
      bodyMedium: TextStyle(color: Color(0xFF230C02), fontSize: 16, fontWeight: FontWeight.normal),
      bodySmall: TextStyle(color: Color(0xFF230C02), fontSize: 10, fontWeight: FontWeight.normal),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(
          const Color(0xFFEDDBC5),
        ),
        backgroundColor: const MaterialStatePropertyAll<Color>(
          Color(0xFF230C02),
        ),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
          ),
        ),
      ),
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(
          const Color(0xFF230C02),
        ),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
          ),
        ),
      ),
    ),
    iconTheme: const IconThemeData(color: Color(0xFF4E2B0F)),
    cardTheme: const CardTheme(
      color: Color(0xFFFFF5E9),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
      ),
    ),
  );
}
