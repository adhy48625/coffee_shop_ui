import 'package:coffee_shop_ui/screen/firstpage.dart';
import 'package:flutter/material.dart';
import 'package:coffee_shop_ui/widget/popup_menu.dart' as  my_popup_menu;

class MyAppBar extends StatelessWidget {
  const MyAppBar({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height / 15,
      width: double.infinity,
      //color: Colors.amber,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 18),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                TextButton.icon(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.search,
                    color: Colors.black,
                  ),
                  label: SizedBox(
                    width: 200,
                    child: TextField(
                      decoration: InputDecoration(
                        hintText: 'Good day, Selenay',
                        hintStyle:
                            Theme.of(context).textTheme.bodyMedium,
                        contentPadding:
                            const EdgeInsets.symmetric(vertical: 10),
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                )
              ],
            ),
            Row(
              children: [
                const Icon(Icons.notification_add),
                IconButton(
                  onPressed: () {},
                  icon: my_popup_menu.CustomPopupMenuButton(
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        bottomLeft: Radius.circular(30),
                      ),
                    ),
                    offset: const Offset(double.maxFinite, 0),
                    color: const Color(0xFFFFF5E9),
                    onSelected: (value) {
                      //print('Selected: $value');
                    },
                    itemBuilder: (BuildContext context) => [
                      PopupMenuItem(
                        value: 'Item 1',
                        child: Row(
                          children: [
                            const Icon(Icons.account_circle),
                            const SizedBox(width: 8),
                            Text(
                              'Profile',
                              style: Theme.of(context).textTheme.bodyMedium,
                            ),
                          ],
                        ),
                      ),
                       const PopupMenuItem(
                        enabled: false,
                        padding: EdgeInsets.all(0.0),
                       height: 0,
                        child: Divider(
                          thickness: 1,
                          color: Color(0xFFEDDBC5),
                        )
                      ),
                      PopupMenuItem(
                        value: 'Item 2',
                        child: Row(
                          children: [
                            const Icon(Icons.settings),
                            const SizedBox(width: 8),
                            Text(
                              'Settings',
                              style: Theme.of(context).textTheme.bodyMedium,
                            ),
                          ],
                        ),
                      ),
                      const PopupMenuItem(
                        enabled: false,
                        padding: EdgeInsets.all(0.0),
                       height: 0,
                        child: Divider(
                          thickness: 1,
                          color: Color(0xFFEDDBC5),
                        )
                      ),
                      PopupMenuItem(
                        value: 'Item 3',
                        child: Row(
                          children: [
                            const Icon(Icons.help_outline),
                            const SizedBox(width: 8),
                            Text(
                              'Help',
                              style: Theme.of(context).textTheme.bodyMedium,
                            ),
                          ],
                        ),
                      ),
                      const PopupMenuItem(
                        enabled: false,
                        padding: EdgeInsets.all(0.0),
                       height: 0,
                        child: Divider(
                          thickness: 1,
                          color: Color(0xFFEDDBC5),
                        )
                      ),
                      PopupMenuItem(
                        
                        value: 'Item 4',
                        child: InkWell(
                          onTap: () => Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const FirstPage()),
                              (route) => false,
                        ),
                          child: Row(
                            children: [
                              const Icon(Icons.logout),
                              const SizedBox(width: 8),
                              Text(
                                'Logout',
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                    child: const Icon(Icons.menu),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}