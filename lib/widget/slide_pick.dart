import 'package:flutter/material.dart';

class SlidePick extends StatelessWidget {
  const SlidePick({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 30),
        //color: Colors.amber,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 18,vertical: 5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: const Color(0xFF230C02),
              ),
              child: const Text('Popular',style: TextStyle(color: Colors.white),),
            ),
            const SizedBox(width: 40),
            const Text('Black coffe'),
            const SizedBox(width: 40),
            const Text('Winter spesial'),
            const SizedBox(width: 40),
            const Text('Decaff'),
            const SizedBox(width: 40),
            const Text('Chocolate'),
          ],
        ),
      ),
    );
  }
}
