import 'package:coffee_shop_ui/model/menu_list.dart';
import 'package:coffee_shop_ui/provider/change_local_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../my_card.dart';
import '../menu.dart';
import '../slide_pick.dart';

class TabBarHome extends ConsumerStatefulWidget {
  const TabBarHome({
    super.key,
  });

  @override
  ConsumerState<TabBarHome> createState() => _TabBarHomeState();
}

class _TabBarHomeState extends ConsumerState<TabBarHome> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Align(
          alignment: Alignment.bottomLeft,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Text(
              'What would you like',
              style: Theme.of(context).textTheme.bodyMedium,
            ),
          ),
        ),
        Align(
          alignment: Alignment.bottomLeft,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Text(
              'to drink today?',
              style: Theme.of(context).textTheme.bodyMedium,
            ),
          ),
        ),
        const SizedBox(height: 30),
        const SlidePick(),
        const SizedBox(height: 30),
        Expanded(
          child: Container(
            color: const Color(0xFF230C02),
            child: ListView.builder(
              itemCount: MenuList.menuList.length,
              itemBuilder: (context, index) {
                return MyCard(
                  id: index,
                  menu: MenuList.menuList[index].coffee.toString(),
                  type: MenuList.menuList[index].type.toString(),
                  assetImage: MenuList.menuList[index].image.toString(),
                  onTap: () {
                    print('tes');
                     Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Menu(
                          id: ref.watch(menuId),
                          favorite: MenuList.menuList[ref.watch(menuId)].favorite!,
                          coffe: MenuList.menuList[ref.watch(menuId)].coffee,
                          coffeType: MenuList.menuList[ref.watch(menuId)].type,
                          price: MenuList.menuList[ref.watch(menuId)].price.toString(),
                          assetImage:MenuList.menuList[ref.watch(menuId)].image),
                    ),
                  );
                  } 
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}
