import 'package:coffee_shop_ui/model/menu_order.dart';
import 'package:coffee_shop_ui/widget/my_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class TabBarCart extends ConsumerStatefulWidget {
  const TabBarCart({
    super.key,
  });

  @override
  ConsumerState<TabBarCart> createState() => _TabBarHomeState();
}

class _TabBarHomeState extends ConsumerState<TabBarCart> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Column(
        children: [
          Align(
            alignment: Alignment.bottomLeft,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: Text(
                'Cart',
                style: Theme.of(context).textTheme.bodyMedium,
              ),
            ),
          ),
          const SizedBox(height: 10),
          Row(
            children: [
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Icon(Icons.shopping_cart_outlined),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 100, right: 30),
                  child: Container(
                    height: 25,
                    decoration: BoxDecoration(
                        border: Border.all(),
                        borderRadius: BorderRadius.circular(9.0)),
                    child: TabBar(
                      labelColor: const Color(0xFFEEDCC6),
                      unselectedLabelColor: const Color(0xFF230C02),
                      indicator: BoxDecoration(
                        border: Border.all(
                          color: const Color(0xFF230C02),
                        ),
                        borderRadius: BorderRadius.circular(8.0),
                        color: const Color(0xFF230C02),
                      ),
                      tabs: const [
                        Tab(text: 'Recenly'),
                        Tab(text: 'Past order'),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(height: 10),
          Expanded(
            child: TabBarView(
              children: [
                Container(
                  color: const Color(0xFF230C02),
                  child: ListView.builder(
                    itemCount: OrderList.orders.length,
                    itemBuilder: (context, index) {
                      return MyCard(
                        id: index,
                        menu: OrderList.orders[index].coffee,
                        type: OrderList.orders[index].type,
                        assetImage: OrderList.orders[index].image,
                        amount: OrderList.orders[index].amount.toString(),
                        date:  OrderList.orders[index].date,
                        onTap: (() {
                          print('tes');
                        }),
                      );
                    },
                  ),
                ),
                const Center(
                  child: Text('Tab 2 Content'),
                ),
              ],
            ),
          ),
          const SizedBox(height: 10),
        ],
      ),
    );
  }
}
