import 'package:coffee_shop_ui/provider/change_local_provider.dart';
import 'package:coffee_shop_ui/screen/mainPage/main_page.dart';
import 'package:coffee_shop_ui/widget/my_appbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../model/menu_favorite.dart';
import '../model/menu_order.dart';

class Menu extends ConsumerStatefulWidget {
  final int id;
  final String coffeType;
  final String coffe;
  final String price;
  final String assetImage;
  final bool favorite;
  const Menu(
      {super.key,
      required this.id,
      required this.coffeType,
      required this.coffe,
      required this.assetImage,
      required this.favorite,
      required this.price});

  @override
  ConsumerState<Menu> createState() => _MenuState();
}

class _MenuState extends ConsumerState<Menu> {
  int numberOrder = 1;
  @override
  Widget build(BuildContext context) {
    final favoriteState = ref.read(favorite(widget.id).notifier);
    MenuFavorite favoriteList;
    favoriteList = MenuFavorite(
      id: widget.id,
      coffee: widget.coffe,
      favorite: widget.favorite,
      favoriteCount: '300',
      image: widget.assetImage,
      type: widget.coffeType,
    );
    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: [
            Column(
              children: [
                const MyAppBar(),
                Container(
                  height: 310,
                  decoration: BoxDecoration(
                    color: Colors.amber,
                    image: DecorationImage(
                      image: AssetImage(widget.assetImage),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ],
            ),
            Align(
              alignment: AlignmentDirectional.bottomEnd,
              child: Container(
                height: MediaQuery.of(context).size.height / 1.9,
                width: MediaQuery.of(context).size.width,
                decoration: const BoxDecoration(
                  color: Color(0xFF4E2B0F),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  ),
                ),
                child: Padding(
                  padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width / 10,
                      right: MediaQuery.of(context).size.width / 10),
                  child: SingleChildScrollView(
                    physics: const BouncingScrollPhysics(),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(height: 20),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(
                              width: 150,
                              child: Text(
                                maxLines: 2,
                                textAlign: TextAlign.start,
                                widget.coffeType,
                                style: const TextStyle(
                                  fontSize: 36,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xFFEEDCC6),
                                ),
                              ),
                            ),
                            IconButton(
                              padding: EdgeInsets.only(
                                  left: MediaQuery.of(context).size.width / 5),
                              onPressed: () {
                                setState(() {
                                  favoriteState.state = !favoriteState.state;
                                  if (favoriteState.state) {
                                    FavoriteList.addFavorite(favoriteList);
                                  } else {
                                    FavoriteList.removeFavorite(widget.id);
                                  }
                                });
                              },
                              icon: favoriteState.state == false
                                  ? const Icon(Icons.favorite_outline,
                                      color: Color(0xFF967259))
                                  : const Icon(Icons.favorite,
                                      color: Colors.red),
                            ),
                            IconButton(
                              onPressed: () {},
                              icon: const Icon(
                                Icons.share,
                                color: Color(0xFF967259),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Text(
                          widget.coffe,
                          style: const TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: Color(0xFFEEDCC6),
                          ),
                        ),
                        const SizedBox(height: 5),
                        const Text(
                          'Complex, yet smooth flavor made to order.',
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.normal,
                            color: Color(0xFFEEDCC6),
                          ),
                        ),
                        const SizedBox(height: 20),
                        Row(
                          children: const [
                            Icon(
                              Icons.star,
                              color: Color(0xFFF2994A),
                            ),
                            SizedBox(width: 5),
                            Text(
                              '4.5',
                              style: TextStyle(
                                color: Color(0xFFF2994A),
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(width: 5),
                            Text(
                              '(10k)',
                              style: TextStyle(
                                color: Color(0xFFF2994A),
                                fontSize: 15,
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 20),
                        const Text(
                          'Size',
                          style: TextStyle(
                            color: Color(0xFFEEDCC6),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: const [
                            SizeCoffeButton(text: '250'),
                            SizeCoffeButton(text: '350'),
                            SizeCoffeButton(text: '450'),
                          ],
                        ),
                        const SizedBox(height: 20),
                        Text(
                          "\$ ${widget.price.toString()}",
                          style: const TextStyle(
                            fontSize: 36,
                            fontWeight: FontWeight.bold,
                            color: Color(0xFFEEDCC6),
                          ),
                        ),
                        const SizedBox(height: 20),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              width: 140,
                              height: 47,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(40),
                                border: Border.all(
                                  color: const Color(0XFFEEDCC6),
                                ),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  TextButton(
                                    child: const Text(
                                      '-',
                                      style:
                                          TextStyle(color: Color(0XFFEEDCC6)),
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        if (numberOrder > 1) {
                                          numberOrder--;
                                        }
                                      });
                                    },
                                  ),
                                  Text(
                                    numberOrder.toString(),
                                    style: const TextStyle(
                                        color: Color(0XFFEEDCC6)),
                                  ),
                                  TextButton(
                                    child: const Text(
                                      '+',
                                      style:
                                          TextStyle(color: Color(0XFFEEDCC6)),
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        numberOrder++;
                                      });
                                    },
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: 140,
                              height: 47,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(40),
                                border: Border.all(
                                  color: const Color(0XFFEEDCC6),
                                ),
                              ),
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    backgroundColor: const Color(0XFFEEDCC6)),
                                onPressed: () {
                                  // Mendapatkan tanggal saat ini
                                  DateTime currentDate = DateTime.now();
                                  // Mendapatkan tanggal dan bulan
                                  int currentDay = currentDate.day;
                                  int currentMonth = currentDate.month;
                                  double price = double.parse(widget.price);
                                  double totalOrderPrice = price * numberOrder;
                                  MenuOrder newOrder = MenuOrder(
                                    amount: numberOrder,
                                    date:
                                        "${currentDay.toString()}/${currentMonth.toString()}",
                                    type: widget.coffeType,
                                    coffee: widget.coffe,
                                    price: totalOrderPrice,
                                    image: widget.assetImage,
                                    favorite: widget.favorite,
                                  );
                                  OrderList.addOrder(newOrder);
                                  ref.read(selectedIndexNavbar.notifier).state =
                                      1;
                                  Navigator.pushAndRemoveUntil(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => const MainPage()),
                                        (route) => false,
                                  );
                                },
                                child: const Text(
                                  'ADD TO CHART',
                                  style: TextStyle(color: Color(0xFF230C02)),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 20)
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SizeCoffeButton extends StatelessWidget {
  final String text;
  const SizeCoffeButton({
    super.key,
    required this.text,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        style: const ButtonStyle(
            shape: MaterialStatePropertyAll(RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(6))))),
        onPressed: () {},
        child: Row(
          children: [
            Image.asset('img/cup.png'),
            const SizedBox(width: 5),
            Text(text)
          ],
        ));
  }
}
