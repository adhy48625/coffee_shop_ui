import 'package:coffee_shop_ui/provider/change_local_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class MyCard extends ConsumerStatefulWidget {
  final int id;
  final String type;
  final String menu;
  final String? date;
  final String? amount;
  final String assetImage;
  final bool? favorite;
  final Function onTap;
  const MyCard({
    super.key,
    required this.id,
    required this.type,
    required this.menu,
    this.amount,
    this.date,
    this.favorite,
    required this.assetImage,
    required this.onTap,
  });
  
  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _CardMenuState();
}

class _CardMenuState extends ConsumerState<MyCard> {
  
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.all(25),
      child: InkWell(
        onTap: (() {
          ref.watch(menuId.notifier).state = widget.id;
          widget.onTap();
        }),
        child: Container(
          height: 125,
          padding: const EdgeInsets.all(16),
          child: Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 15),
                  Text(widget.type,
                      style: Theme.of(context).textTheme.bodySmall),
                  const SizedBox(height: 10),
                  Text(widget.menu,
                      style: Theme.of(context).textTheme.bodyMedium),
                  widget.favorite == true ? Icon(Icons.favorite,color: Colors.red[400],) : Text(widget.amount ?? '',
                      style: Theme.of(context).textTheme.bodySmall)
                ],
              ),
              Positioned(
                left: 230,
                child: Text(widget.date ?? '',
                    style: Theme.of(context).textTheme.bodySmall),
              ),
              Positioned(
                left: 260,
                child: Image(
                  image: AssetImage(widget.assetImage),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
