// ignore_for_file: file_names

import 'package:coffee_shop_ui/screen/login/login_page.dart';
import 'package:flutter/material.dart';

class FirstPage extends StatefulWidget {
  const FirstPage({super.key});
  @override
  State<FirstPage> createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  @override
  Widget build(BuildContext context) {
    final buttonSize = MediaQuery.of(context).size.width / 1.5;
    final container = MediaQuery.of(context).size.height / 2.5;
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage("img/coffe.png"),
          fit: BoxFit.fill,
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: container,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 26),
              child:
                  Text('Welcome', style: Theme.of(context).textTheme.bodyLarge),
            ),
            Text(
              '   Back!',
              style: Theme.of(context).textTheme.bodyLarge,
            ),
            const SizedBox(height: 20),
            Align(
              child: SizedBox(
                width: buttonSize,
                child: ElevatedButton(
                  child: const Text('Login'),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const LoginPage()),
                    );
                  },
                ),
              ),
            ),
            Align(
              child: SizedBox(
                width: buttonSize,
                child: OutlinedButton(
                  child: const Text('Create an account'),
                  onPressed: () {},
                ),
              ),
            ),
            Align(
              child: TextButton(
                onPressed: () {},
                child: const Text(
                  'Forgot your password?',
                  style: TextStyle(color: Colors.black),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
