// ignore_for_file: file_names

import 'package:coffee_shop_ui/screen/mainPage/main_Page.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});
  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String email = '';
  String password = '';
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool validateEmail = false;
  bool validatePassword = false;

  @override
  Widget build(BuildContext context) {
    final buttonSize = MediaQuery.of(context).size.width / 1.5;
    final textField = MediaQuery.of(context).size.width / 1.2;
    final container = MediaQuery.of(context).size.height / 2.5;

    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage("img/coffe.png"),
          fit: BoxFit.fill,
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: container,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 26),
                child: Text('Welcome',
                    style: Theme.of(context).textTheme.bodyLarge),
              ),
              Text(
                '   Back!',
                style: Theme.of(context).textTheme.bodyLarge,
              ),
              Align(
                child: SizedBox(
                  width: textField,
                  child: TextField(
                      controller: emailController,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        labelText: 'Email',
                        errorText:
                            validateEmail ? "Value Can't Be Empty" : null,
                      ),
                      onChanged: (text) {
                        setState(() {
                          text.isEmpty
                              ? validateEmail = true
                              : validateEmail = false;
                          email = text;
                        });
                      }),
                ),
              ),
              Align(
                child: SizedBox(
                  width: textField,
                  child: TextField(
                      controller: passwordController,
                      keyboardType: TextInputType.visiblePassword,
                      decoration: InputDecoration(
                        labelText: 'Password',
                        errorText:
                            validatePassword ? "Value Can't Be Empty" : null,
                      ),
                      onChanged: (text) {
                        setState(() {
                          text.isEmpty
                              ? validatePassword = true
                              : validatePassword = false;
                          password = text;
                        });
                      }),
                ),
              ),
              const SizedBox(height: 20),
              Align(
                child: SizedBox(
                  width: buttonSize,
                  child: ElevatedButton(
                    child: const Text('Login'),
                    onPressed: () {
                      if (email.isEmpty) {
                        setState(() {
                          
                        validateEmail = true;
                        });
                      } else {
                        setState(() {
                          
                        validateEmail = false;
                        });
                      }

                      if (password.isEmpty) {
                        setState(() {
                          
                        validatePassword = true;
                        });
                      } else {
                        setState(() {
                          
                        validatePassword = false;
                        });
                      }

                      if (email != '' && password != '') {
                        Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const MainPage()),
                              (route) => false,
                        );
                      }
                    },
                  ),
                ),
              ),
              Align(
                child: SizedBox(
                  width: buttonSize,
                  child: OutlinedButton(
                    child: const Text('Create an account'),
                    onPressed: () {},
                  ),
                ),
              ),
              Align(
                child: TextButton(
                  onPressed: () {},
                  child: const Text(
                    'Forgot your password?',
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
