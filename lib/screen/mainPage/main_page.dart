import 'package:coffee_shop_ui/widget/widgetTabBar/tab_bar_cart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../../provider/change_local_provider.dart';
import '../../widget/my_appbar.dart';
import '../../widget/widgetTabBar/tab_bar_favorite.dart';
import '../../widget/widgetTabBar/tab_bar_home.dart';

class MainPage extends ConsumerStatefulWidget {
  const MainPage({super.key});

  @override
  ConsumerState<MainPage> createState() => _MainPageState();
}

class _MainPageState extends ConsumerState<MainPage> {
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  //Tambah konten navbar di sini
  static const List<Widget> _widgetOptions = <Widget>[
    TabBarHome(),
    TabBarCart(),
    TabBarFavorite(),
    Text(
      'Index 3: account',
      style: optionStyle,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: SizedBox(
          height: MediaQuery.of(context).size.height / 11,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 6,
                width: 35,
                margin: ref.read(selectedIndexNavbar) == 0
                    ? const EdgeInsets.only(left: 50)
                    : ref.read(selectedIndexNavbar) == 1
                        ? const EdgeInsets.only(left: 140)
                        : ref.read(selectedIndexNavbar) == 2
                            ? const EdgeInsets.only(left: 235)
                            : const EdgeInsets.only(left: 330),
                decoration: const BoxDecoration(
                  color: Color(0xFF230C02),
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(25),
                    bottomRight: Radius.circular(25),
                  ),
                ),
              ),
              BottomNavigationBar(
                elevation: 0,
                items: const <BottomNavigationBarItem>[
                  BottomNavigationBarItem(
                    icon: Icon(
                      Icons.home,
                      color: Color(0xFF4E2B0F),
                    ),
                    label: '',
                    backgroundColor: Color(0xFFEDDBC5),
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(Icons.shopping_cart_outlined,
                        color: Color(0xFF4E2B0F)),
                    label: '',
                    backgroundColor: Color(0xFFEDDBC5),
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(Icons.favorite_border, color: Color(0xFF4E2B0F)),
                    label: '',
                    backgroundColor: Color(0xFFEDDBC5),
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(Icons.account_circle_outlined,
                        color: Color(0xFF4E2B0F)),
                    label: '',
                    backgroundColor: Color(0xFFEDDBC5),
                  ),
                ],
                currentIndex: ref.watch(selectedIndexNavbar) ,
                selectedItemColor: Colors.amber[800],
                onTap: (value) {
                  ref.read(selectedIndexNavbar.notifier).state = value;
                },
              ),
            ],
          ),
        ),
        body: Column(
          children: [
            const MyAppBar(),
            Expanded(
              child: _widgetOptions.elementAt(ref.read(selectedIndexNavbar)),
            )
          ],
        ),
      ),
    );
  }
}
