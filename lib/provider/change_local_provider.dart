import 'package:flutter_riverpod/flutter_riverpod.dart';

final menuId = StateProvider((ref) => 0);
final date = StateProvider((ref) => '');
final amount = StateProvider((ref) => 0);
final selectedIndexNavbar = StateProvider((ref) => 0);
final favorite = StateProvider.family<bool,int>((ref,index) => false);
