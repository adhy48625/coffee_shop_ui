class MenuItem {
  final int id;
  final String type;
  final String coffee;
  final double price;
  final String image;
  final int? amount;
  final String? date;
  final bool? favorite;

  MenuItem({
    required this.id,
    required this.type,
    required this.coffee,
    required this.price,
    required this.image,
    this.amount,
    this.date,
    this.favorite,
  });
}

class MenuList {
  static List<MenuItem> menuList = [
    MenuItem(
      id: 1,
      type: 'Black coffee',
      coffee: 'ICED AMERICANO',
      price: 4.0,
      image: 'img/icedAmericano.png',
      favorite : false,
    ),
    MenuItem(
      id: 2,
      type: 'Winter special',
      coffee: 'CAPPUCINO LATTE',
      price: 5.0,
      image: 'img/latte.png',
      favorite : false,
    ),
    MenuItem(
      id: 3,
      type: 'DECAFF',
      coffee: 'SILKY CAFEAU LAIT',
      price: 6.0,
      image: 'img/silky.png',
      favorite : false,
    ),
    MenuItem(
      id: 4,
      type: 'CHOCOLATE',
      coffee: 'Iced Chocolate',
      price: 7.0,
      image: 'img/icedChocolate.png',
      favorite : false,
    ),
  ];
}
