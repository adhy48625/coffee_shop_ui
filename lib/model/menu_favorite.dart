class MenuFavorite {
  final int id;
  final String favoriteCount;
  final String type;
  final String coffee;
  final String image;
   bool favorite;

  MenuFavorite({
    required this.id,
    required this.favoriteCount,
    required this.type,
    required this.coffee,
    required this.image,
    required this.favorite,
  });
}

class FavoriteList {
  static List<MenuFavorite> favorite = []; 

  static void addFavorite(MenuFavorite order) {
    favorite.add(order);
  }

  static void removeFavorite(int id) {
    favorite.removeWhere((favorite) => favorite.id == id);
  }
}
