class MenuOrder {
  //final int id;
  final int amount;
  final String date;
  final String type;
  final String coffee;
  final double price;
  final String image;
  final bool favorite;

  MenuOrder({
    //required this.id,
    required this.amount,
    required this.date,
    required this.type,
    required this.coffee,
    required this.price,
    required this.image,
    required this.favorite,
  });
}

class OrderList {
  static List<MenuOrder> orders = []; 

  static void addOrder(MenuOrder order) {
    orders.add(order);
  }

  static void removeOrder(int index) {
    orders.removeAt(index);
  }
}
